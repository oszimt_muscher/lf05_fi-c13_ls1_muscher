import java.util.Scanner;
public class Schaltjahr {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		int eingabeJahr;
		boolean istSchaltjahr;
		
		System.out.println("Bitte geben Sie ihr Jahr ein: ");
		eingabeJahr = tastatur.nextInt();
		
		istSchaltjahr = schaltjahr(eingabeJahr);
		
		if (istSchaltjahr == true) {
			System.out.println("Das Jahr " + eingabeJahr + " ist ein Schaltjahr.");
		}
		
		else if (istSchaltjahr == false) {
			System.out.println(" Das Jahr " + eingabeJahr + " ist kein Schaltjahr.");
		}
		

	}
	
	public static boolean schaltjahr(int x) {
		
		if (x % 4 != 0) {
			return false;
		}
		else if (x % 400 == 0) {
			return true;
		}
		else if ( x % 100 == 0) {
			return false;
		}
		else {
			return true;
		}
		
	}

}
