import java.util.Scanner;

class Fahrkartenautomat2
{
	

	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		  
		  int ticketAnzahl;
		  double ticketPreis = 0;
		  double zuZahlenderBetrag = 0.0;
		  int ticketAuswahl;
		  
		  
		  
		  
		  System.out.println(""
		  		+ "1	Einzelfahrschein Berlin AB           2,90\n"
		  		+ "2	Einzelfahrschein Berlin BC	         3,30\n"
		  		+ "3	Einzelfahrschein Berlin ABC          3,60\n"
		  		+ "4	Kurzstrecke	                         1,90\n"
		  		+ "5	Tageskarte Berlin AB                 8,60\n"
		  		+ "6	Tageskarte Berlin BC                 9,00\n"
		  		+ "7	Tageskarte Berlin ABC                9,60\n"
		  		+ "8	Kleingruppen-Tageskarte Berlin AB   23,50\n"
		  		+ "9	Kleingruppen-Tageskarte Berlin BC   24,30\n"
		  		+ "10	Kleingruppen-Tageskarte Berlin ABC  24,90\n");
		  
	      System.out.print("Ihre Wahl: ");
		  ticketAuswahl = tastatur.nextInt();
		  
		  
		 double [] preis = {0, 2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9};
		 
		
	      System.out.print("Waehlen Sie die Anzahl der Tickets (1 bis 10): ");
	       ticketAnzahl = tastatur.nextInt();
		  
	       if(ticketAnzahl <1 || ticketAnzahl >10) {
	    	   System.out.println("Es ist nur eine Ticketanzahl von 1 bis 10 Tickets mglich. Es wird mit einer Ticketanzahl von 1 fortgesetzt.");
	    	   ticketAnzahl = 1;
	       }
		  
	       zuZahlenderBetrag = ticketAnzahl * preis[ticketAuswahl];
		  
		   return zuZahlenderBetrag;}
	
	
	public static double fahrkartenBezahlen(double a) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < a)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (a - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
	    	   eingeworfeneMuenze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMuenze;
	       }
	       
	       double rueckgabeBetrag = eingezahlterGesamtbetrag - a;
	       return rueckgabeBetrag;
	}
	
	public static void warte(int millisekunde) {
		
	}
	
    public static void fahrkartenAusgeben() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	}
	
	public static double rueckgeldAusgeben(double b) {
		
		double rueckgabebetrag= b;
		
		if(rueckgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Muenzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	       }
		      return b;
		
	}
	
	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rueckgabeBetrag;
		boolean wiederspielen = true;
	
		
	while (wiederspielen == true) {
		
		int userEingabe;
		
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rueckgabeBetrag);
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wuenschen Ihnen eine gute Fahrt.");
		
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println();
		System.out.println("Wollen Sie noch mehr Tickets kaufen?(Ja=1 und Nein=2): ");
		
		userEingabe = tastatur.nextInt();
		
            if (userEingabe==1) {
			
			wiederspielen = true;	
		    }
            
            else {
			
			wiederspielen=false;
			System.out.println("Vielen Dank! Auf Wiedersehen!");
			
		}
	}
	}

}