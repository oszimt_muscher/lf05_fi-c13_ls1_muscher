import java.util.Scanner;

public class OhmschesGesetz {

	public static void main(String[] args) {
		
		char buchstabe;
		double r;
		double i;
		double u;
		
		System.out.println("Bitte geben Sie an, was Sie ausrechnen wollen.(R, I, U)");
		Scanner scanner = new Scanner(System.in);
		buchstabe = scanner.next().charAt(0);
		
		if(buchstabe == 'R') {
			System.out.println("Bitte geben Sie die Spannung U ein: ");
			u = scanner.nextDouble();
			System.out.println("Bitte geben Sie die Stromst�rke I ein: ");
			i = scanner.nextDouble();
			r = u / i;
			System.out.println("Der Widerstand betr�gt: " + r + " Ohm.");
		}
		
		else if (buchstabe == 'I') {
			System.out.println("Bitte geben Sie den Widerstand R ein: ");
			r = scanner.nextDouble();
			System.out.println("Bitte geben Sie die Spannung U ein: ");
			u = scanner.nextDouble();
			i = u / r;
			System.out.println("Die Stromst�rke betr�gt: " + i + " Ampere.");
		}
		else if (buchstabe == 'U') {
			System.out.println("Bitte geben Sie den Widerstand R ein: ");
			r = scanner.nextDouble();
			System.out.println("Bitte geben Sie die Stromst�rke I ein: ");
			i = scanner.nextDouble();
			u = i * r;
			System.out.println("Die Spannung betr�gt: " + u + " Volt.");
		}
		else {
			System.out.println("Fehler!");
		}
		

	}

}
