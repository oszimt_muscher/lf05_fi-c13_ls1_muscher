import java.util.Scanner;
public class BMI {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		
		int gewicht;
		double gr��e;
		double bmi;
		char geschlecht;
		
		System.out.println("Bitte geben Sie Ihr Gewicht in kg an (z.B. 75): ");
		gewicht = tastatur.nextInt();
		
		System.out.println("Bitte geben Sie ihre Gr��e in m an (z.B. 1,87): ");
		gr��e = tastatur.nextDouble();
		
		bmi = gewicht / (gr��e * gr��e);
		
		System.out.println("Ihr BMI betr�gt: " + bmi);
		
		System.out.println("Sind Sie m�nnlich oder weiblich? Geben Sie bitte m oder w ein: ");
		
		geschlecht = tastatur.next().charAt(0);
		
		if (geschlecht == 'm') {
			
			if (bmi < 20) {
				
				System.out.println("Sie sind untergewichtig.");
			}
			else if(bmi < 25) {
				
				System.out.println("Sie sind im Normalgewicht.");
			}
			else {
				System.out.println("Sie sind �bergewichtig.");
			}
		}
		
		else if (geschlecht == 'w') {
			
			if (bmi < 19) {
				
				System.out.println("Sie sind untergewichtig.");
			}
			else if(bmi < 24) {
				
				System.out.println("Sie sind im Normalgewicht.");
			}
			else {
				System.out.println("Sie sind �bergewichtig.");
			}
			
		}

	}

}
