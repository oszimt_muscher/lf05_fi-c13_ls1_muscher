﻿import java.util.Scanner;

class Fahrkartenautomat
{
	
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		  
		  int ticketAnzahl;
		  double zuZahlenderBetrag;
		  System.out.print("Zu zahlender Betrag (EURO): ");
	       zuZahlenderBetrag = tastatur.nextDouble();

	      System.out.print("Anzahl der Tickets: ");
	       ticketAnzahl = tastatur.nextInt();
	       
	       zuZahlenderBetrag = ticketAnzahl * zuZahlenderBetrag;
	      return zuZahlenderBetrag;
	}
	public static double fahrkartenBezahlen(double a) {
		
		Scanner tastatur = new Scanner(System.in);
		
		double eingezahlterGesamtbetrag;
		double eingeworfeneMuenze;
		
		
		eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < a)
	       {
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (a - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, hoechstens 2 Euro): ");
	    	   eingeworfeneMuenze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMuenze;
	       }
	       
	       double rueckgabeBetrag = eingezahlterGesamtbetrag - a;
	       return rueckgabeBetrag;
	}
	public static void fahrkartenAusgeben() {
		 System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	}
	
	public static double rueckgeldAusgeben(double b) {
		
		double rueckgabebetrag= b;
		
		if(rueckgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rueckgabebetrag in Hoehe von " + rueckgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Muenzen ausgezahlt:");

	           while(rueckgabebetrag >= 2.0) // 2 EURO-MÃ¼nzen
	           {
	        	  System.out.println("2 EURO");
		          rueckgabebetrag -= 2.0;
	           }
	           while(rueckgabebetrag >= 1.0) // 1 EURO-MÃ¼nzen
	           {
	        	  System.out.println("1 EURO");
		          rueckgabebetrag -= 1.0;
	           }
	           while(rueckgabebetrag >= 0.5) // 50 CENT-MÃ¼nzen
	           {
	        	  System.out.println("50 CENT");
		          rueckgabebetrag -= 0.5;
	           }
	           while(rueckgabebetrag >= 0.2) // 20 CENT-MÃ¼nzen
	           {
	        	  System.out.println("20 CENT");
	 	          rueckgabebetrag -= 0.2;
	           }
	           while(rueckgabebetrag >= 0.1) // 10 CENT-MÃ¼nzen
	           {
	        	  System.out.println("10 CENT");
		          rueckgabebetrag -= 0.1;
	           }
	           while(rueckgabebetrag >= 0.0499)// 5 CENT-MÃ¼nzen
	           {
	        	  System.out.println("5 CENT");
	 	          rueckgabebetrag -= 0.05;
	           }
	       }
		      return b;
		
	}
	
   
	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rueckgabeBetrag;
		
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rueckgabeBetrag);
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wÃ¼nschen Ihnen eine gute Fahrt.");
	}	
}