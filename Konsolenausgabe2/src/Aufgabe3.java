
public class Aufgabe3 {

	public static void main(String[] args) {
		
		double F1 = -20;
		double F2 = -10;
		double F3 =   0;
		double F4 =  20;
		double F5 =  30;
		
		double C1 = -28.8889;
		double C2 = -23.3333;
		double C3 = -17.7778;
		double C4 = -6.6667;
		double C5 = -1.1111;
		
		System.out.printf("%1s", "Fahrenheit");
		System.out.printf("%3s %-5s","|" , "Celsius" );

	}

}
