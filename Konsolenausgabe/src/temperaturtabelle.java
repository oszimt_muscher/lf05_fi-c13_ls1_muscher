
public class temperaturtabelle {

	public static void main(String[] args) {
		
		
		
		
		double F1 = -28.8889;
		double F2 = -23.3333;
		double F3 = -17.7778;
		double F4 = -6.6667;
		double F5 = -1.1111;
		
		int C1 = -20;
		int C2 = -10;
		int C3 = 0;
		int C4 = 20;
		int C5 = 30;
		
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%11s", "Celsius\n");
		System.out.println("-----------------------");
		System.out.printf("%-12s", C1);
		System.out.print("|");
		System.out.printf("%3s %.2f\n", " ", F1);
		System.out.printf("%-12s", C2);
		System.out.print("|");
		System.out.printf("%3s %.2f\n", " ", F2);
		System.out.printf("%-12s", C3);
		System.out.print("|");
		System.out.printf("%3s %.2f\n", " ", F3);
		System.out.printf("%-12s", C4);
		System.out.print("|");
		System.out.printf("%4s %.2f\n", " ", F4);
		System.out.printf("%-12s", C5);
		System.out.print("|");
		System.out.printf("%4s %.2f\n", " ", F5);
		
	}
}