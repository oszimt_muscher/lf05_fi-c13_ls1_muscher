import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		double zahl1;
		double zahl2;
		char rechenoperation;
		double ergebnis;
		
		System.out.println("Bitte geben Sie ihre erste Zahl ein: ");
		zahl1 = scanner.nextDouble();
		
		System.out.println("Bitte geben Sie ein, welche Rechenoperation durchgeführt werden soll: ");
		rechenoperation = scanner.next().charAt(0);
		
		System.out.println("Bitte geben Sie ihre zweite Zahl ein: ");
		zahl2 = scanner.nextDouble();
		
		if (rechenoperation == '+') {
			ergebnis = zahl1 + zahl2;
			System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
		}
		else if(rechenoperation == '-') {
			ergebnis = zahl1 - zahl2;
			System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnis);
		}
		else if( rechenoperation == '*') {
			ergebnis = zahl1 * zahl2;
			System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis);
		}
		else if (rechenoperation == '/') {
			ergebnis = zahl1 / zahl2;
			System.out.println(zahl1 + " / " + zahl2 + " = " + ergebnis);
		}
		else {
			System.out.println("Fehler!");
		}

	}

}
